package brainfuck.compiler

import java.io.File
import java.util.*

object BrainfuckCompiler {
    fun compile(source: String, output: File) {
        val writer = AssemblyWriter(output)
        val loopStack = Stack<Int>()
        loopStack.push(0)

        for (char in source) {
            when (char) {
                '>' -> writer.moveForward()
                '<' -> writer.moveBackwards()
                '+' -> writer.increment()
                '-' -> writer.decrement()
                '.' -> writer.printCurrent()
                ',' -> TODO("Reading value not implemented")
                '[' -> {
                    assert(loopStack.size >= 1) { "There was a problem, stack should never be empty" }
                    val level = loopStack.size
                    val count = loopStack.pop()!! + 1

                    writer.jumpOnZero(level, count)

                    loopStack.push(count)
                    loopStack.push(0)
                }
                ']' -> {
                    assert(loopStack.size >= 2) { "There was a problem, possibly closed parentheses without opening them first" }
                    loopStack.pop()
                    val level = loopStack.size
                    val count = loopStack.peek()!!

                    writer.jumpOnNonZero(level, count)
                }
            }
        }
        writer.finish()
    }
}
