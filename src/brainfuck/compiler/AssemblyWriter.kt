package brainfuck.compiler

import java.io.File

internal class AssemblyWriter(val output: File) {
    private val OPEN_LABEL = "open"
    private val CLOSE_LABEL = "close"
    private val MAX_MEMORY_LOCATION = 10
    private val STEP = 1
    private val ARRAY = "a"
    private val R1 = "bx"

    var memoryLocation = 0

    init {
        output.appendText(".intel_syntax noprefix\n")
        output.appendText("\n")
        output.appendText(".data\n")
        output.appendText("""
a0: .long 0
a1: .long 0
a2: .long 0
a3: .long 0
a4: .long 0
a5: .long 0
a6: .long 0
a7: .long 0
a8: .long 0
a9: .long 0
""")
        output.appendText("\n")
        output.appendText(".text\n")
        output.appendText(".globl _start\n")
        output.appendText("_start:\n")
    }

    fun increment() {
        output.appendText("INC $R1\n")
    }
    fun decrement() {
        output.appendText("DEC $R1\n")
    }
    fun moveForward() {
        output.appendText("MOV $ARRAY$memoryLocation,$R1\n")
        if (memoryLocation >= MAX_MEMORY_LOCATION - STEP) memoryLocation = 0
        else memoryLocation += STEP
        output.appendText("MOV $R1,$ARRAY$memoryLocation\n")
    }
    fun moveBackwards() {
        output.appendText("MOV $ARRAY$memoryLocation,$R1\n")
        if (memoryLocation <= 0) memoryLocation = MAX_MEMORY_LOCATION - STEP
        else memoryLocation -= STEP
        output.appendText("MOV $R1,$ARRAY$memoryLocation\n")
    }
    fun jumpOnZero(level: Int, count: Int) {
        output.appendText("${OPEN_LABEL}_${concatLevelCount(level, count)}:\n")
        output.appendText("CMP $R1, 0\n")
        output.appendText("JZ ${CLOSE_LABEL}_${concatLevelCount(level, count)}\n")
    }
    fun jumpOnNonZero(level: Int, count: Int) {
        output.appendText("${CLOSE_LABEL}_${concatLevelCount(level, count)}:\n")
        output.appendText("CMP $R1, 0\n")
        output.appendText("JNZ ${OPEN_LABEL}_${concatLevelCount(level, count)}\n")
    }
    fun printCurrent() {
        output.appendText("""
            PUSH $R1
            MOV rax, 1 # write
            MOV rdi, 1 # to stdout
            MOV rsi, rsp # ???
            MOV rdx, 1 # x char
            SYSCALL
            POP $R1
            
        """.trimIndent())
    }

    fun finish() {
        output.appendText("\n")
        output.appendText("MOV rax, 60\n")
        output.appendText("XOR rdi, rdi\n")
        output.appendText("SYSCALL\n")
    }

    private fun concatLevelCount(level: Int, count: Int): String {
        return "${level}_${count}"
    }
}
