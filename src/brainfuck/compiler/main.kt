package brainfuck.compiler
import java.io.File
import java.io.InputStreamReader

private const val FILE_NAME = "compiled"

fun main(args: Array<String>) {
    when(args.size) {
        0 -> println("ERROR: No file name given")
        1 -> compile(args[0])
        else -> println("ERROR: Too many arguments, only 1 is needed")
    }
}

private fun compile(filePath: String) {
    val file = File(filePath)
    val output = File("$FILE_NAME.s")
    output.writeText("")
    BrainfuckCompiler.compile(file.readText(), output)

    println("\nCompiling")
    val compiling = Runtime.getRuntime().exec("gcc -c $FILE_NAME.s")
    while (compiling.isAlive) {}
    if(compiling.exitValue() != 0) {
        for (s in InputStreamReader(compiling.errorStream).readLines()) {
            println(s)
        }
        System.exit(compiling.exitValue())
    }

    println("\nLinking")
    val linking = Runtime.getRuntime().exec("ld $FILE_NAME.o -o $FILE_NAME")
    while (linking.isAlive) {}
    if(linking.exitValue() != 0) {
        for (s in InputStreamReader(linking.errorStream).readLines()) {
            println(s)
        }
        System.exit(linking.exitValue())
    }

    println("\nRunning")
    val run = Runtime.getRuntime().exec("./compiled")
    for ( s in InputStreamReader(run.inputStream).readLines()) { for (c in s) print(" " + c.toInt())
    println()}

}
