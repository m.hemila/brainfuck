package brainfuck

object ParenthesisFinder {
    internal fun findMatchingClosingIndex(source: String, index: Int): Int {
        val relevantSource = source.substring(index + 1, source.length)
        val nextOpener = relevantSource.indexOf('[')
        val nextCloser = relevantSource.indexOf(']')
        return index + 1 + when {
            nextCloser == -1 -> throw RuntimeException("Opening parentheses does not have matching closing parentheses. Poor source.")
            nextOpener == -1 -> nextCloser
            nextOpener > nextCloser -> nextCloser
            else -> findMatchingClosingIndex(relevantSource, nextCloser + 1)
        }
    }

    internal fun findMatchingOpeningIndex(source: String, index: Int): Int {
        val relevantSource = source.substring(0, index)
        val previousOpener = relevantSource.lastIndexOf('[')
        val previousCloser = relevantSource.lastIndexOf(']')
        return when {
            previousOpener == -1 -> throw RuntimeException("Closing parentheses does not have matching opening parentheses. Poor source.")
            previousCloser == -1 -> previousOpener
            previousCloser < previousOpener -> previousOpener
            else -> findMatchingOpeningIndex(relevantSource, previousOpener)
        }
    }
}
