package brainfuck.interpreter

internal class FunctionalMemory private constructor(private val memory: List<Int>) : Memory {
    internal constructor(): this(List(30000) { 0 })

    override fun next(): FunctionalMemory {
        val head = memory.first()
        val tail = memory.drop(1)
        return FunctionalMemory(tail + head)
    }
    override fun previous(): FunctionalMemory {
        val last = memory.last()
        val body = memory.dropLast(1)
        return FunctionalMemory(listOf(last) + body)
    }
    override fun increment(): FunctionalMemory {
        val tail = memory.drop(1)
        val head = memory.first()
        return FunctionalMemory(listOf(head + 1) + tail)
    }
    override fun decrement(): FunctionalMemory {
        val tail = memory.drop(1)
        val head = memory.first()
        return FunctionalMemory(listOf(head - 1) + tail)
    }
    override fun getCurrentChar(): Char {
        return memory.first().toChar()
    }
    override fun getCurrentValue(): Int {
        return memory.first()
    }
    override fun currentIsZero(): Boolean {
        return memory.first() == 0
    }
}
