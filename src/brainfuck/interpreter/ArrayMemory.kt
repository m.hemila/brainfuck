package brainfuck.interpreter

internal class ArrayMemory : Memory {
    private val memory = IntArray(30000)
    private var index = 0

    override fun next(): Memory {
        if (index == memory.size - 1) index = 0
        else index += 1
        return this
    }
    override fun previous(): Memory {
        if (index == 0) index = memory.size - 1
        else index -= 1
        return this
    }
    override fun increment(): Memory {
        memory[index] += 1
        return this
    }
    override fun decrement(): Memory {
        memory[index] -= 1
        return this
    }
    override fun getCurrentChar(): Char {
        return memory[index].toChar()
    }
    override fun getCurrentValue(): Int {
        return memory[index]
    }
    override fun currentIsZero(): Boolean {
        return memory[index] == 0
    }

    override fun toString(): String {
        return memory.contentToString()
    }
}
