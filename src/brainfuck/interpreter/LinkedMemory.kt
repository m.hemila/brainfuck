package brainfuck.interpreter

import java.util.*

internal class LinkedMemory : Memory {
    private val memory = LinkedList(List(30000) {0})

    override fun next(): LinkedMemory {
        val first = memory.removeFirst()!!
        memory.addLast(first)
        return this
    }
    override fun previous(): LinkedMemory {
        val last = memory.removeLast()!!
        memory.addFirst(last)
        return this
    }
    override fun increment(): LinkedMemory {
        val first = memory.removeFirst()!!
        memory.addFirst(first + 1)
        return this
    }
    override fun decrement(): LinkedMemory {
        val first = memory.removeFirst()!!
        memory.addFirst(first - 1)
        return this
    }
    override fun getCurrentChar(): Char {
        return memory.first!!.toChar()
    }
    override fun getCurrentValue(): Int {
        return memory.first!!
    }
    override fun currentIsZero(): Boolean {
        return memory.first!! == 0
    }

    override fun toString(): String {
        return memory.joinToString()
    }
}
