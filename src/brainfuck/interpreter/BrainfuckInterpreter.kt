package brainfuck.interpreter

import brainfuck.ParenthesisFinder

/*
    > 	Move the pointer to the right
    < 	Move the pointer to the left
    + 	Increment the memory cell at the pointer
    - 	Decrement the memory cell at the pointer
    . 	Output the character signified by the cell at the pointer
    , 	Input a character and store it in the cell at the pointer
    [ 	Jump past the matching ] if the cell at the pointer is 0
    ] 	Jump back to the matching [ if the cell at the pointer is nonzero
*/

object BrainfuckInterpreter {
    fun interpret(source: String): String {
        var ret = ""
        var index = 0
        var memory = FunctionalMemory()

        while (index < source.length) {
            when(source[index]) {
                '>' -> memory = memory.next()
                '<' -> memory = memory.previous()
                '+' -> memory = memory.increment()
                '-' -> memory = memory.decrement()
                '.' -> ret += memory.getCurrentChar()
                ',' -> TODO("Reading value not implemented")
                '[' -> if (memory.currentIsZero()) index = ParenthesisFinder.findMatchingClosingIndex(source, index)
                ']' -> if (!memory.currentIsZero()) index = ParenthesisFinder.findMatchingOpeningIndex(source, index)
            }
            index += 1
        }

        return ret
    }
}
