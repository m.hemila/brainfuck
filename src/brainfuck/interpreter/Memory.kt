package brainfuck.interpreter

interface Memory {
    fun next(): Memory
    fun previous(): Memory
    fun increment(): Memory
    fun decrement(): Memory
    fun getCurrentChar(): Char
    fun getCurrentValue(): Int
    fun currentIsZero(): Boolean
}
