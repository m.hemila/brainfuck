package brainfuck.interpreter

import java.io.File

fun main(args: Array<String>) {
    when(args.size) {
        0 -> println("ERROR: No file name given")
        1 -> interpret(args[0])
        else -> println("ERROR: Too many arguments, only 1 is needed")
    }
}

private fun interpret(filePath: String) {
    val file = File(filePath)
    print(BrainfuckInterpreter.interpret(file.readText()))
}
