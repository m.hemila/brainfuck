package brainfuck.interpreter

import brainfuck.interpreter.ArrayMemory
import brainfuck.interpreter.FunctionalMemory
import brainfuck.interpreter.LinkedMemory
import brainfuck.interpreter.Memory
import org.junit.jupiter.api.Test
import kotlin.reflect.KClass
import kotlin.reflect.full.createInstance
import kotlin.test.assertEquals

class MemoryTest {
    @Test
    fun `test LinkedMemory`() {
        val memoryTestSuite = MemoryTestSuite(LinkedMemory::class)
        memoryTestSuite.runTests()
    }
    @Test
    fun `test ArrayMemory`() {
        val memoryTestSuite = MemoryTestSuite(ArrayMemory::class)
        memoryTestSuite.runTests()
    }
    @Test
    fun `test FunctionalMemory`() {
        val memoryTestSuite = MemoryTestSuite(FunctionalMemory::class)
        memoryTestSuite.runTests()
    }
}

internal class MemoryTestSuite(val memoryClass: KClass<out Memory>) {
    fun runTests() {
        `should increment by one`()
        `should decrement by one`()
        `should handle simple movement`()
        `should handle complex movement`()
    }
    private fun newInstance(): Memory {
        return memoryClass.createInstance()
    }
    private fun `should increment by one`() {
        var memory = newInstance()
        memory = memory.increment()
        assertEquals(1, memory.getCurrentValue())
    }
    private fun `should decrement by one`() {
        var memory = newInstance()
        memory = memory.decrement()
        assertEquals(-1, memory.getCurrentValue())
    }
    private fun `should handle simple movement`() {
        var memory = newInstance()
        memory = memory.increment()
        memory = memory.next()
        memory = memory.previous()
        assertEquals(1, memory.getCurrentValue())
    }
    private fun `should handle complex movement`() {
        var memory = newInstance()
        memory = memory.increment()
        memory = memory.increment()
        memory = memory.next()
        memory = memory.increment()
        memory = memory.increment()
        memory = memory.increment()
        memory = memory.previous()
        assertEquals(2, memory.getCurrentValue())
        memory = memory.next()
        assertEquals(3, memory.getCurrentValue())
        memory = memory.next()
        assertEquals(0, memory.getCurrentValue())
    }
}
