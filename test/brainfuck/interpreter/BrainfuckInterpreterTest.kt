package brainfuck.interpreter
import brainfuck.interpreter.BrainfuckInterpreter
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class BrainfuckInterpreterTest {
    @Test
    fun `run helloWorld`() {
        val ret = BrainfuckInterpreter.interpret("++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.\n")
        assertEquals("Hello World!\n", ret)
    }
}

