package brainfuck

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class ParenthesisFinderTest {
    internal class FindMatchingClosingIndex {
        @Test
        fun `findMatchingClosingIndex should return correct index simple`() {
            assertEquals(4, ParenthesisFinder.findMatchingClosingIndex("++[+]+++", 2))
        }

        @Test
        fun `findMatchingClosingIndex should return correct index from 2 levels`() {
            assertEquals(42, ParenthesisFinder.findMatchingClosingIndex("++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.", 2))
        }

        @Test
        fun `findMatchingClosingIndex should return correct index from 3 levels`() {
            assertEquals(42, ParenthesisFinder.findMatchingClosingIndex("++[>++[+[>++>+++>+++>+<<<<-]>+>+>]>>+[<]<-]>>.", 2))
        }
    }

    internal class FindMatchingOpeningIndex {
        @Test
        fun `findMatchingClosingIndex should return correct index simple`() {
            assertEquals(3, ParenthesisFinder.findMatchingOpeningIndex("+++[+]++", 5))
        }

        @Test
        fun `findMatchingClosingIndex should return correct index for 2 levels`() {
            assertEquals(2, ParenthesisFinder.findMatchingOpeningIndex("++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.", 42))
        }

        @Test
        fun `findMatchingClosingIndex should return correct index simple long`() {
            assertEquals(43, ParenthesisFinder.findMatchingOpeningIndex("++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-", 45))
        }

        @Test
        fun `findMatchingClosingIndex should return correct index from 3 levels`() {
            assertEquals(2, ParenthesisFinder.findMatchingClosingIndex("++[>++[+[>++>+++>+++>+<<<<-]>+>+>]>>+[<]<-]>>.", 42))
        }
    }
}
